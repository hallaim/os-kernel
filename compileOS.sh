# make an image file of a floppy disk that is filled with zeros
dd if=/dev/zero of=floppya.img bs=512 count=2880

# copy bootload to the beginning of floppya.img
dd if=bootload of=floppya.img bs=512 count=1 conv=notrunc

# copy map to the floppya.img sector 1
dd if=map.img of=floppya.img bs=512 count=1 seek=1 conv=notrunc

# copy dir to floppya.img sector 2
dd if=dir.img of=floppya.img bs=512 count=1 seek=2 conv=notrunc

# places msg, test1, and test2 filse onto disk using loadFile
./loadFile msg
./loadFile test1
./loadFile test2

# compiles and links Shell.c to lib.asm
bcc -ansi -c -o Shell.o Shell.c
as86 lib.asm -o lib_asm.o
ld86 -o Shell -d Shell.o lib_asm.o

# compiles and links fib to lib.asm
bcc -ansi -c -o fib.o fib.c
as86 lib.asm -o lib_asm.o
ld86 -o fib -d fib.o lib_asm.o

# places Shell on the disk using loadfile
./loadFile Shell

# places fib on the disk using loadfile
./loadFile fib

# compile kernel
bcc -ansi -c -o kernel.o kernel.c

# assemble kernel.asm
as86 kernel.asm -o kernel_asm.o

# links together to produde file 'kernel'
ld86 -o kernel -d kernel.o kernel_asm.o

# copy kernel to floppya.img at sector 3
dd if=kernel of=floppya.img bs=512 conv=notrunc seek=3
