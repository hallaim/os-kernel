/* ACADEMIC INTEGRITY PLEDGE                                              */
/*                                                                        */
/* - I have not used source code obtained from another student nor        */
/*   any other unauthorized source, either modified or unmodified.        */
/*                                                                        */
/* - All source code and documentation used in my program is either       */
/*   my original work or was derived by me from the source code           */
/*   published in the textbook for this course or presented in            */
/*   class.                                                               */
/*                                                                        */
/* - I have not discussed coding details about this project with          */
/*   anyone other than my instructor. I understand that I may discuss     */
/*   the concepts of this program with other students and that another    */
/*   student may help me debug my program so long as neither of us        */
/*   writes anything during the discussion or modifies any computer       */
/*   file during the discussion.                                          */
/*                                                                        */
/* - I have violated neither the spirit nor letter of these restrictions. */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* Signed:___Michael Hallai________________________ Date:___3/11/16        */
/*                                                                        */
/*                                                                        */
/* 3460:4/526 BlackDOS kernel, Version 1.02, Spring 2016.                 */

void handleInterrupt21(int, int, int, int);

void main()
{
   /* sets up the interrupt 33 service routine */
   makeInterrupt21();
   
   /* clear the screen */
   interrupt(33,12,1,10,0);

   /* BlackDOS ascii banner */
   interrupt(33,0,"___.   .__                 __       .___           \r\n\0",0,0);
   interrupt(33,0,"\\_ |__ |  | _____    ____ |  | __ __| _/___  ______\r\n\0",0,0);
   interrupt(33,0," | __ \\|  | \\__  \\ _/ ___\\|  |/ // __ |/ _ \\/  ___/\r\n\0",0,0);
   interrupt(33,0," | \\_\\ \\  |__/ /\\ \\\\  \\___|    </ /_/ ( <_> )___ \\ \r\n\0",0,0);
   interrupt(33,0," |___  /____(____  /\\___  >__|_ \\____ |\\___/____  >\r\n\0",0,0);
   interrupt(33,0,"     \\/          \\/     \\/     \\/    \\/         \\/ \r\n\0",0,0);
   interrupt(33,0," V. 1.02, C. 2016. Based on a project by M. Black. \r\n\0",0,0);
   interrupt(33,0," Author(s): Michael Hallai.\r\n\r\n\0",0,0);

   /* Run the shell */
   interrupt(33,4,"Shell\0",2,0); 
   interrupt(33,0,"Bad or missing command interpreter or shell.\r\n\0",0,0);

   while (1) ;
}

/* Prints a string to the screen:
   Interrupt 16 with 14 (0xE) in the AH register
   the ASCII character stored in the AL register 
   prints to the screen at the current cursor location */
void printString(char* c)
{
   char al;
   char ah;
   int ax;
   int pos = 0;

   while(1){
      al = c[pos];
      ah = 14;
      ax = ah*256 + al;
      /* Don't print string terminator character */
      if(al=='\0')
         break;
      else
         interrupt(16, ax, 0, 0, 0);
      pos = pos + 1;
   }

   return;
}

/* Reads a string from keyboard:
   Interrupt 22 returns an ASCII character.
   Stops when the user presses enter, enter key is not stored */
void readString(char* c)
{
   char al, ah=0;
   char* ch;
   int ax=0, pos=0;

   while(1)
   {
      /* gets character */
      ch = interrupt(22,  0, 0, 0, 0); 

      /* break out of loop if enter is pressed */
      if(ch=='\r'){
         break;
      }
      else{
         if(ch!='\b'){
            /* store char in c[] */
            c[pos] = ch;
            pos = pos + 1;
            /* print to screen */
            ah = 14;  ax = ah*256 + ch;  interrupt(16, ax, 0, 0, 0);
         }
         else if(pos > 0){
            pos = pos - 1;
            /* print backspace */
            interrupt(16, 14*256+ch, 0, 0, 0);
            /* print space */
            interrupt(16, 14*256+' ', 0, 0, 0);
            /* print '\b' to screen */
            ah = 14;  ax = ah*256 + ch;  interrupt(16, ax, 0, 0, 0);
         }
      }
   } /*endloop*/

   /* add string terminator to the end of string */
   c[pos] = '\0';
   return;
}

/* Clears the screen:
   repositions the cursor in the upper left-hand corner (coordinates (0,0)) 
   bx: background color
   cx: foreground color */
void clearScreen(int bx, int cx)
{
   int i = 0;
   int ax;
   /* Issue 24 newlines */
   for(i=0; i<24; i++)
   {
      ax = 14*256 + '\n';
      interrupt(16, ax, 0, 0, 0);
   }

   interrupt(16, 512, 0, 0, 0);

   if((bx>0) && (cx>0) && (bx<=8) && (cx<=16)){
      interrupt(16,1536,4096 * (bx-1) + 256 * (cx-1),0,6223);
   }
   return;
}

int mod(int a, int b)
{
   int x = a;
   while (x >= b) x = x - b;
   return x;
}

int div(int a, int b)
{
   int q = 0;
   while (q * b <= a) q++;
   return (q - 1);
}

/* Prints an int to the screen: */
void writeInt(int x)
{
   char number[6], *d;
   int q = x, r;
   if (x < 1)
   {
      d = number; *d = 0x30; d++; *d = 0x0; d--;
   }
   else
   {
      d = number + 5;
      *d = 0x0; d--;
      while (q > 0)
      {
         r = mod(q,10); q = div(q,10);
         *d = r + 48; d--;
      }
      d++;
   }
   printString(d);
   return; /* I added this return statement */
}

/* Reads an int from keyboard:
   Interrupt 22 returns an ASCII character.
   Stops when the user presses enter, enter key is not stored 
   Then loops through array keeping a total int as the end integer value */
void readInt(int* number)
{
   char al, ah=0;
   char* ch;
   int ax=0, pos=0, x, total=0;
   char numstr[6];

   while(1)
   {
      /* gets number as a character */
      ch = interrupt(22,  0, 0, 0, 0); 

      /* break out of loop if enter is pressed */
      if(ch=='\r'){
         break;
      }
      else{
         if(ch!='\b'){
            /* store char in numstr[] */
            numstr[pos] = ch;
            pos = pos + 1;
            /* print to screen */
            ah = 14;  ax = ah*256 + ch;  interrupt(16, ax, 0, 0, 0);
         }
         else if(pos > 0){
            pos = pos - 1;
            /* print backspace */
            interrupt(16, 14*256+ch, 0, 0, 0);
            /* print space */
            interrupt(16, 14*256+' ', 0, 0, 0);
            /* print '\b' to screen */
            ah = 14;  ax = ah*256 + ch;  interrupt(16, ax, 0, 0, 0);
         }
      }
   } /*endloop*/

   /* add to end string */
   numstr[pos] = '\0';

   /* loop through numstr[] */
   pos = 0;
   while(numstr[pos]!='\0')
   {
      /* x gets ineger value */
      x = numstr[pos] - '0';
      /* add to total */
      total = total*10 + x;
      pos = pos + 1;
   }
   *number = total;
   return;
}

/* read sectors from floppya into a buffer */
void readSector(char* buffer, int sector)
{
   int DL = 0;   /* device #, zero for floppya */
   int AX = 513; /* AH * 256 + AL */

   int relSecNo = (mod(sector,18)) + 1;
   int headNo = mod((div(sector,18)), 2);
   int trackNo = div(sector,36);

   int CX = trackNo * 256 + relSecNo;
   int DX = headNo * 256 + DL;

   interrupt(19, AX, buffer, CX, DX);
   return;
}

/* write sectors from buffer to disk */
void writeSector(char* buffer, int sector)
{
   int DL = 0;   /* device #, zero for floppya */
   int AX = 769; /* AH * 256 + AL */

   int relSecNo = (mod(sector,18)) + 1;
   int headNo = mod((div(sector,18)), 2);
   int trackNo = div(sector,36);

   int CX = trackNo * 256 + relSecNo;
   int DX = headNo * 256 + DL;

   interrupt(19, AX, buffer, CX, DX);
   return;
}

/* Find's a substring in a string, or more specifically, a filename
   in the directory. Returns -1 if not found. */
int find(char directory[], char fname[]) 
{
   int firstChar; /* location in directory[] of the first char of fname[] */
   int i = 0; /* location in directory[] */
   int j = 0; /* location in fname[] */
 
   while (i < 512) {
 
      /* find an instance of the first char of fname matching a char in dir */
      while (directory[i] != fname[0] && i < 512)
         i++;
 
      /* if at the end of directory, then the filename was not found */
      if (i == 511)
         return (-1);
 
      firstChar = i;
 
      /* move through fname and directory until the chars dont match */
      while (directory[i] == fname[j] && i < 512 && fname[j] != '\0') {
         i++;
         j++;
      }
 
      /* If at the end of fname, then return the location of the first char */
      if (fname[j] == '\0')
         return (firstChar);
      /* if at the end of directory, then the filename was not found 
      if (i == 512)
         return (-1);*/
 
      i = firstChar + 1; /* move to next char in directory */

      j = 0; /* reset the position in fname */
   }
   return (-1);
}

/* delete a file from the disk */
void deleteFile(char* name)
{
   char dir[512];
   char map[512];
   int i, loc;
   int fnameLocation = -1; /* location of fname in directory */
   int sectorNum;

   /* get directory sector */
   interrupt(33,2,dir,2,0);

   /* get map sector */
   interrupt(33,2,map,1,0);

   /* look for file name */
   fnameLocation = find(dir, name);
   
   /* if the name was found in directory */
   if ((fnameLocation != -1) && (fnameLocation != 0)){
      loc = fnameLocation + 6;

      /* Set the first byte of the file name to zero */
      dir[fnameLocation] = 0;

      /* set the corresponding map bytes to zero */
      for (i=0;i<26;i++){
         map[dir[loc+i]] = 0;
      }

      /* write back modified map and dir */
      writeSector(map,1);
      writeSector(dir,2);
   }
   else{/* else, the filename was not found in the directory */
      /* send error file not found */
      printString("NOT FOUND ERROR");
      interrupt(33,15,0,0,0);
   }
   return;
}

/* takes a character array containing a file name and 
   reads the file into a provided buffer */
void readFile(char* fname, char* buffer, int* size)
{
   char *ptr = &buffer[0];
   char directory[512];
   int i = 0; /* size in sectors */
   int fnameLocation = -1; /* location of fname in directory */
   int sectorNum;

   /* get directory sector */
   interrupt(33,2,directory,2,0);

   /* look for file name */
   fnameLocation = find(directory, fname);

   /* if the fname was found in directory */
   if (fnameLocation != -1){

      /* start of file sector */
      sectorNum = directory[fnameLocation+6];

      /* loop to load each sector of file */
      while(sectorNum != 0x0)
      {
         /* load sector into buffer */
         interrupt(33,2,ptr,sectorNum,0);

         /* get sector number of next section of file */
         i = i + 1;
         sectorNum = directory[fnameLocation+6+i];
         
         /* move to the next section of the buffer */
         ptr = ptr + 512;
      }
   }
   /* else, the filename was not found in the directory */
   else{
      /* send error file not found */
      interrupt(33,15,0,0,0);
   }

   /* size is changed to the number of sectors the file consists of */
   *size = i;
}

/* write a file to the disk */
void writeFile(char* name, char* buffer, int numberOfSectors)
{
   char map[512];
   char directory[512];
   char tempbuffer[512];
   int fileloc = -1;
   int dirloc = 0;
   int found = 0;
   int i, j, k;
   int nameLength = 0;
   int sector = 0;

   /* Load the map and directory sectors */
   readSector(map,1);
   readSector(directory,2);

   /* See if file already exists with same name */
   fileloc = find(directory, name);

   if(fileloc > 0){
      interrupt(33,15,1,0,0);
   }
   else{
      /* find a free directory entry */
      for (dirloc = 0; dirloc<16; dirloc++){
         if (directory[32*dirloc] == 0x00){
            found = 1;
            break;
         }
      }

      /* If no free directory location, error */
      if(found == 0){
         interrupt(33,15,2,0,0);
      }
      else{
         /* Count num of chars in name */
         nameLength=0;
         while((name[nameLength] != 0x0) && (name[nameLength] != '\0')){
            nameLength++;
         }
         /* Put the name in the directory */
         for (i=0; i<nameLength; i++){
            directory[32*dirloc+i] = name[i];
         }
         /* pad with zeros */
         while(i<6){
            directory[32*dirloc+i] = 0;
            i++;
         }

         /* For each sector */
         for (i=0; i<numberOfSectors; i++)
         {
            /* Find a free sector by searching through the map for a zero */
            sector = 0;
            while(map[sector] != 0x0){
               sector++;
            }
            if(sector==255){
               interrupt(33,15,2,0,0);
            }
            /* Set that sector to 255 in the map */
            map[sector] = 255;
            /* Add that sector number to the file's directory entry. */
            directory[32*dirloc+6+i] = sector;
            /* Write 512 bytes from the buffer holding the file to that sector*/
            for(j=0; j<512; j++){
               tempbuffer[j] = buffer[(512*i)+j];
            }
            /* write tempbuffer to the sector */
            writeSector(tempbuffer,sector);
         }
         /* Fill in the remaining bytes in the directory entry with zeros. */
         for(j=i; j<26; j++){
            directory[32*dirloc+6+j] = 0;
         }
         /* Write the map and directory sectors back to the disk. */
         writeSector(map,1);
         writeSector(directory,2);
      }
   }
}


/* Loads a process into memory and executes it */
void runProgram(char* name, int segment)
{
   int size = 0;
   int i;
   char buffer[13312];
   int offset = 0;

   int baseLocation = segment * 0x1000;

   /* load file into buffer usin readfile() */
   interrupt(33,3,name,buffer,&size);

   if(size!=0){
      /* load buffer into memory */
      for(i=0; i<13312; i++)
      {
         putInMemory(baseLocation, i, buffer[i]);
      }
      /* Call launchProgram(int) */
      launchProgram(baseLocation);
   }
}

/* Terminate program system call */
void stop()
{
   char name[6];
   name[0]='S';name[1]='h';name[2]='e';name[3]='l';name[4]='l';name[5]=0x0;
   interrupt(33,4,name,2,0);
   /*launchProgram(8192);*/
}

/* accepts an error number as a parameter and prints out the
   corresponding error message */
void error(int bx)
{
   char zero[16];
   char one[15];
   char two[11];
   char defaulterr[15];

   zero[0] = 'F'; zero[1] = 'i'; zero[2] = 'l';    zero[3] = 'e';
   zero[4] = ' '; zero[5] = 'n'; zero[6] = 'o'; zero[7] = 't'; zero[8] = ' ';
   zero[9] = 'f'; zero[10] = 'o'; zero[11] = 'u'; zero[12] = 'n'; zero[13] = 'd';
   zero[14] = '.'; zero[15] = '\0';

   one[0] = 'B'; one[1] = 'a'; one[2] = 'd';    one[3] = ' ';
   one[4] = 'f'; one[5] = 'i'; one[6] = 'l'; one[7] = 'e'; one[8] = ' ';
   one[9] = 'n'; one[10] = 'a'; one[11] = 'm'; one[12] = 'e'; one[13] = '.';
   one[14] = '\0';

   two[0] = 'D'; two[1] = 'i'; two[2] = 's';    two[3] = 'k';
   two[4] = ' '; two[5] = 'f'; two[6] = 'u'; two[7] = 'l'; two[8] = 'l';
   two[9] = '.'; two[10] = '\0';

   defaulterr[0] = 'G'; defaulterr[1] = 'e'; defaulterr[2] = 'n'; defaulterr[3] = 'e';
   defaulterr[4] = 'r'; defaulterr[5] = 'a'; defaulterr[6] = 'l'; defaulterr[7] = ' '; 
   defaulterr[8] = 'e'; defaulterr[9] = 'r'; defaulterr[10] = 'r'; defaulterr[11] = 'o'; 
   defaulterr[12] = 'r'; defaulterr[13] = '.'; defaulterr[14] = '\0';

   switch(bx) {

      case 0 :
         interrupt(33,0,zero,0,0);
         break;

      case 1 :
         interrupt(33,0,one,0,0);
         break;

      case 2 :
         interrupt(33,0,two,0,0);
         break;

      default :
         interrupt(33,0,defaulterr,0,0);
         break;
   }
}

/* interrupt 33 handler provides all developed services:
      AX:   BX:                  CX:               Action:

      0     Address of string                      printString(bx)
      1     Address of char[]                      readString(bx)
      2     Address of char[]  
      3     Address of char[]  
      4     Address of char[]  
      5                                            stop()
      12    BG color (1-8)       FG color (1-16)   clearScreen(bx,cx)
      13    Int to print                           writeInt(bx)
      14    address of int var                     readInt(bx)
      15    error number                           error(bx)
*/
void handleInterrupt21(int ax, int bx, int cx, int dx)
{
   switch(ax) {

   case 0 :
      printString(bx);
      break;

   case 1 :
      readString(bx);
      break;

   case 2 :
      readSector(bx, cx);
      break;

   case 3 :
      readFile(bx, cx, dx);
      break;

   case 4 :
      runProgram(bx, cx);
      break;

   case 5 :
      stop();
      break;

   case 6 :
      writeSector(bx,cx);
      break;

   case 7 :
      deleteFile(bx);
      break;

   case 8 :
      writeFile(bx,cx,dx);
      break;

   case 11 :
      /* Reboot DOS */
      interrupt(25,0,0,0,0);
      break;

   case 12:
      clearScreen(bx,cx);
      break;

   case 13:
      writeInt(bx);
      break;

   case 14:
      readInt(bx);
      break;

   case 15:
      error(bx);
      break;
  
   default :
      break;
   }
}
