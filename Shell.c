#include "blackdos.h"

void command(char*);

void main()
{
   char input[512];

   /* clear the screen */
   interrupt(33,12,1,10,0);

   /* loop through getting commands*/
   while(1){
      PRINTS("cxxxx][===blackdos===>");
      SCANS(input);
      PRINTS("\r\n");
      command(input);
      PRINTS("\r\n");
   }
   END;
}

/* parses and executes commands */
void command(char* input)
{
   int i, j, size;
   int total = 0;
   char buffer[13312];
   char dir[512];
   char str[512];
   char fname1[7];
   char fname2[7];

   /* boot */
   if(input[0]=='b' && input[1]=='o' && input[2]=='o' && input[3]=='t')
      interrupt(33,11,0,0,0);

   /* cls */
   else if(input[0]=='c' && input[1]=='l' && input[2]=='s')
   {
      for(i=0; i<25; i++){
         PRINTS("\r\n");
      }
      interrupt(33,12,1,10,0);
   }

   /* del */
   else if(input[0]=='d' && input[1]=='e' && input[2]=='l')
   {
      for(i=0; i<6; i++){
         fname1[i] = input[4+i];
      }
      fname1[6] = '\0';

      interrupt(33,7,fname1,0,0);
   }

   /* dir */
   else if(input[0]=='d' && input[1]=='i' && input[2]=='r')
   {
      /* read in directory sector */
      interrupt(33,2,dir,2,0);

      total = 0;

      for(i=0; i<16; i++)
      {
         if(dir[32*i] != 0)
         {
            for(j=0; j<6; j++)
            {
               if(dir[32*i+j]=='\0')
                  fname1[j]=' ';
               else
                  fname1[j]=dir[32*i+j];
            }
            fname1[6]='\0';
            for(size=0; size<=26; size++)
            {
               if(dir[32*i+6+size]=='\0')
                  break;
            }
            total = total + size;
            PRINTS(fname1);
            PRINTS(" \0");
            PRINTN(size);
            PRINTS("\r\n");
         }
      }
      PRINTS("\r\nUSED   ");
      PRINTN(total);
      PRINTS("\r\nFREE   ");
      PRINTN(256 - total);
      PRINTS("\r\n");
   }

   /* echo */
   else if(input[0]=='e' && input[1]=='c' && input[2]=='h' && input[3]=='o')
   {
      str[0]='\0';
      if(input[4]!='\0')
      {
         for(i=0; input[5+i]!='\0'; i++)
         {
            str[i]=input[5+i];
         }
         str[i]='\0';
         PRINTS(str);
         
         str[0]='\0';
      }
      PRINTS("\r\n");
   }

   /* type */
   else if(input[0]=='t' && input[1]=='y' && input[2]=='p' && input[3]=='e')
   {
      for(i=0; i<6; i++){
         fname1[i] = input[5+i];
      }
      fname1[6] = '\0';

      buffer[0]='\0';
      interrupt(33,3,fname1,buffer,0);
      PRINTS(buffer);
   }

   /* tweet */
   else if(input[0]=='t' && input[1]=='w' && input[2]=='e' && input[3]=='e' && input[4]=='t')
   {
      for(i=0; i<6; i++){
         fname1[i] = input[6+i];
      }
      fname1[6] = '\0';

      PRINTS("File Contents (less than 140 characters) :\r\n\0");
      SCANS(buffer);

      interrupt(33,8,fname1,buffer,1);
   }

   /* run */
   else if(input[0]=='r' && input[1]=='u' && input[2]=='n')
   {
      for(i=0; i<6; i++){
         fname1[i] = input[4+i];
      }
      fname1[6] = '\0';

      interrupt(33,4,fname1,2,0);
   }

   /* copy */
   else if(input[0]=='c' && input[1]=='o' && input[2]=='p' && input[3]=='y')
   {
      for(i=0; (input[5+i]!=' ') && (input[5+i]!='\0'); i++){
         fname1[i] = input[5+i];
      }
      fname1[i] = '\0';
      i++;
      for(j=0; (input[5+i]!=' ') && (input[5+i]!='\0'); i++){
         fname2[j] = input[5+i];
         j++;
      }
      fname2[j] = '\0';

      interrupt(33,3,fname1,buffer,0);
      interrupt(33,8,fname2,buffer,1);
   }

   /* help */
   else if(input[0]=='h' && input[1]=='e' && input[2]=='l' && input[3]=='p')
   {
      interrupt(33,12,1,10,0);
      PRINTS("USER GUIDE\r\n\0");

      PRINTS("  boot\r\n\0");
      PRINTS("    Reboot the system and restart the o.s.\r\n\0");

      PRINTS("  cls\r\n\0");
      PRINTS("    Clear the screen.\r\n\0");

      PRINTS("  copy file1 file2\r\n\0");
      PRINTS("    Create file2 and copy file1 to file2.\r\n\0");

      PRINTS("  del filename\r\n\0");
      PRINTS("    Delete the named file.\r\n\0");

      PRINTS("  dir\r\n\0");
      PRINTS("    List the directory contents, as well as file sizes, and free space.\r\n\0");

      PRINTS("  echo comment\r\n\0");
      PRINTS("    Display comment on screen.\r\n\0");

      PRINTS("  help\r\n\0");
      PRINTS("    Display the user manual.\r\n\0");

      PRINTS("  run filename\r\n\0");
      PRINTS("    Run an executable file.\r\n\0");

      PRINTS("  tweet filename\r\n\0");
      PRINTS("    Create a text file with the name filename.\r\n\0");

      PRINTS("  type filename\r\n\0");
      PRINTS("    Print out contents of the file filename.\r\n\0");
   }

   else
      PRINTS("Command Not Found");
}